/**
 * SPDX-PackageName: kwaeri/filesystem-session-store
 * SPDX-PackageVersion: 1.0.2
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'


// INCLUDES
import { ClientSession, SessionStore, BaseSessionStore, SessionBits } from '@kwaeri/session-store';
import { Filesystem, FilesystemEntry, FilesystemRecord } from '@kwaeri/filesystem';
import { kdt } from '@kwaeri/developer-tools';
import * as fs from 'fs';
import debug from 'debug';


// DEFINES
const _     = new kdt(),
      DEBUG = debug( 'nodekit:filesystem-session-store' );


/**
 * Filesystem Session Store
 *
 * Manages a 'store' object, where each file is a session named after its 'ID'.
 * For each file, an object that is the session, and all of its
 * associated information, is stored within as a JSON string.
 */
export class FilesystemSessionStore implements SessionStore {
    /**
     * @var { string }
     */
    public version: string;

    /**
     * @var { string }
     */
    public type: string;

    /**
     * @var { any }
     */
    protected configuration: any;

    /**
     * @var { Filesystem }
     */
    private filesystem: Filesystem;

    /**
     * Class constructor
     *
     * @param { any } store The store interface. In this case, a filesystem IO interface
     * @param { any } configuration The session store configuration
     *
     * @returns { void }
     */
    constructor( configuration: any ) {
        // Pack the store interface into the configuration:
        //configuration = _.set( configuration, 'store', new Filesystem() );
        //super( configuration );

        this.filesystem = new Filesystem();

        // Organize all of the uncertainty:
        this.version     = ( configuration && configuration.version ) ? configuration.version : null,
        this.type        = ( configuration && configuration.type ) ? configuration.type : "Filesystem";

        this.configuration = configuration;

        // Now it's time to run self-diagnostics and setup:
        this.init();
    }


    /**
     * Checks that the filesystem is ready as a session store, and if not
     * prepares it (simply checks that the configured directories exist, and
     * if not, creates them).
     *
     * @param void
     *
     * @returns { Promise<void> }
     */
    private async init(): Promise<void> {
        // Ensure that we have the proper directory structure in place:
        const dataPath = 'data',
              cachePath = _.get( this.configuration.paths, 'cache', `${dataPath}/cache` );

        let   dataDirectoryExists,
              createdDataDirectory,
              cacheDirectoryExists,
              createdCacheDirectory;

        // Check that the data path doesn't already exist
        if( !( await this.filesystem.exists( dataPath ) ) ) {
            // As long as it does not already exist... we can create it:
            if( !( await this.filesystem.createDirectory( dataPath ) ) )
                return Promise.reject( new Error( '[FILESYSTEM_SESSION_STORE] There was an error creating the data directory. ' ) );
        }

        if( !( await this.filesystem.exists( cachePath ) ) ) {
            if( !( await this.filesystem.createDirectory( cachePath ) ) )
                return Promise.reject( new Error( '[FILESYSTEM_SESSION_STORE] There was an error creating the cache directory. ' ) );
        }

        DEBUG( `Filesystem session store initialized successfully` );

        return Promise.resolve();
    }


    /**
     * Method which checks if a session exists on disk
     *
     * @param { string } id The id of the session to find
     *
     * @returns { Promise<boolean> } True if the sesion exists, false if it doesn't
     */
    private async find( id: string ): Promise<boolean>
    {
        const ci          = this,
            cachePath   = _.get( this.configuration.paths, 'cache', 'data/cache' ),
            sessionPath = `${cachePath}/${id}`;

        DEBUG( `Check that session '%s' exists [FIND]`, id );

        if( !( await this.filesystem.exists( sessionPath ) ) ) {
            DEBUG( `Session '%s' does not exist [FIND]`, id );

            return Promise.resolve( false );
        }

        return Promise.resolve( true );
    }


    /**
     * A method to read a Sesssion from the filesystem
     *
     * @param { void }
     *
     * @return { Promise<SessionBits|boolean> }
     */
    private async read( id: string ): Promise<SessionBits|boolean>
    {
        const ci             = this,
              cachePath      = _.get( this.configuration.paths, 'cache', 'data/cache' ),
              sessionPath    = `${cachePath}/${id}`;

        DEBUG( `Check that session '%s' exists [READ]`, sessionPath );

        // Next thing to do is to check whether the conf does exist:
        if( !( await this.filesystem.exists( sessionPath ) ) ) {
            DEBUG( `Session '%s' does not exist [READ]`, sessionPath );
            return Promise.resolve( false );
        }

        DEBUG( `Read session '%s'`, sessionPath );

        // If everything 'exists', Read in the configuration:
        return new Promise(
            ( resolve, reject ) => {
                fs.readFile(
                    sessionPath,
                    ( error, data ) => {
                        if( error )
                            reject( new Error( `[FILESYSTEM_SESSION_STORE] There was an issue reading the session from ${sessionPath}: ${error.message}. ` ) );

                        DEBUG( `Parse session '%s' found at '%s'`, id, cachePath);

                        // Parse the file's contents into our KwaeriConfiguration:
                        if( data && data.length ) {
                            const session = JSON.parse( data as any );

                            // Log it for now for proof:
                            DEBUG( session );

                            resolve(  session );
                        }
                        else {
                            // Log it for now for proof:
                            DEBUG( data );

                            resolve(  data as any );
                        }
                    }
                );
            }
        );
    }


    /**
     * A method to write the session to the filesystem
     *
     * @param { any } id The name of the session to use as a file name
     * @param { any } session The session data to write to file
     *
     * @return { Promise<any> }
     */
    private async write( id: string, session: any ): Promise<boolean> {
        const ci              = this,
              cachePath       = _.get( this.configuration.paths, 'cache', 'data/cache' ),
              sessionPath     = `${cachePath}/${id}`;

        try {
            DEBUG( `Write session '${sessionPath}'` );

            // Write the configuration file
            return Promise.resolve( await this.filesystem.createFile( cachePath, id, JSON.stringify( session ) ) );
        }
        catch( error ) {
            return Promise.reject( new Error( `[FILESYSTEM_SESSION_STORE] There was an issue writing session file '${sessionPath}': ${error}. ` ) );
        }
    }


    /**
     * Method to delete a session file from the filesystem
     *
     * @param { string } id The session id the file being deleted is named after
     *
     * @returns { Promise<T extends FilesystemPromise> }
     */
    public async delete( id: string ): Promise<boolean> {
        const ci              = this,
              cachePath       = _.get( this.configuration.paths, 'cache', 'data/cache' ),
              sessionPath     = `${cachePath}/${id}`;

        try {
            DEBUG( `Delete session '%s'`, sessionPath );

            // delete the configuration file
            return Promise.resolve( await this.filesystem.remove( sessionPath ) );
        }
        catch( error ) {
            return Promise.reject( new Error( `[FILESYSTEM_SESSION_STORE] There was an issue deleting the session file: ${error}. ` ) );
        }
    }


    /**
     * Creates a new client session
     *
     * @param { string } id The id of the session to create
     * @param { any } clientSession The object representation of a client session
     *
     * @returns { any }
     */
    public async createSession( id: string, sessionBits: SessionBits = {} as SessionBits ): Promise<ClientSession> {
        DEBUG( `Create session '%s'`, id );

        // When we want to create a session file, first write the file:
        //this.store[id] = clientSession;
        if( !( await this.write( id, sessionBits ) ) )
            DEBUG( `Error creating session '${id}': session could not be written to disk` );

        DEBUG( `Read session '%s' after write`, id );

        // Then return the read session, to ensure it was written, etc:
        return Promise.resolve(  await this.read( id ) as ClientSession  );
    }


    /**
     * Returns an object from memory representing a specific session
     *
     * @param { string | number } id The id of the session requested
     *
     * @returns { Promise<ClientSession|null> } the promise for a {@link ClientSession} or `null`.
     */
    public async getSession( id: string ): Promise<ClientSession|null> {
        DEBUG( `Get session '%s'`, id );

        const deferredRead = await this.read( id );

        DEBUG( `Session found and read: ['%o']`, deferredRead );

        //return ( ( this.store.hasOwnProperty( id ) ) ? this.store[id] : false );
        return Promise.resolve( ( ( deferredRead ) ? deferredRead as ClientSession : null ) );
    };


    /**
     * Removes the specified session from memory
     *
     * @param { string } id The id of the session to delete
     *
     * @returns { Promise<boolean> } the promise for a `boolean`
     */
    public async deleteSession( id: string ): Promise<boolean> {
        DEBUG( `Search for session '%s' pending delete`, id );

        if( await this.find( id ) ) {
            DEBUG( `Delete session '%s'`, id );

            return Promise.resolve( await this.delete( id ) );
        }

        DEBUG( `Session not found when attempting to delete session '%s'`, id );

        return Promise.resolve( false );
    }


    /**
     * Gets a count of existing sessions
     *
     * @param void
     *
     * @returns { Promise<any> }
     */
    public async countSessions(): Promise<any> {
        const ci              = this,
              cachePath       = _.get( this.configuration.paths, 'cache', 'data/cache' );

        DEBUG( `Get list of sessions [COUNT]` );

        const deferredList = await this.filesystem.list( cachePath );

        if( !deferredList || !deferredList.length )
            return Promise.reject( new Error( `Error getting a list of sessions from '${cachePath}'. ` ) );

        return Promise.resolve( deferredList.length );
    }


    /**
     * Deletes any sessions which have gone beyond their expiration
     *
     * @param void
     *
     * @returns { Promise<void> }
     */
    public async cleanSessions(): Promise<void> {
        const ci              = this,
              cachePath       = _.get( this.configuration.paths, 'cache', 'data/cache' ),
              ts              = new Date();

        DEBUG( `Get list of sessions [CLEAN]` );

        const deferredList = await this.filesystem.list( cachePath );

        DEBUG( `Remove expired sessions` );

        for( const session in deferredList ) {
            const sessionBits = await this.read( `${deferredList[session].name}` );

            const stamp = _.get( sessionBits, 'expires' );

            if( ts > stamp )
                this.deleteSession( deferredList[session].name );
        }

        return Promise.resolve();
    }


    /**
     * Gets the specified value from the specified session
     *
     * @param { string | number } id The id of the session the value is being requested from
     * @param { string } name The name of the session value that is being requested
     * @param { any } defaultValue A default value in the event the property being requested is unknown or unset
     *
     * @returns { any } the promise for a value
     */
    public async get( id: string, name: string, defaultValue: any ): Promise<any> {
        DEBUG( `Get '%s' from session '%s'`, name, id );

        const deferredRead = await this.read( id );

        if( !deferredRead ) {
            DEBUG( `Error getting '%s' from '%s'`, name, id );

            return Promise.reject( new Error( `Error getting '${name}' from session '${id}': session not found. ` ) );
        }

        return Promise.resolve( _.get( deferredRead, name, null ) );
    }


    /**
     * Sets the specified value of the specified session
     *
     * @param { string: number } id The id of the session for which the value is being set
     * @param { string } name The name of the property for the specified session for which the value is being set
     * @param { any } value The value being set for the specified property of the specified session
     *
     * @returns { any } the promise for a value
     */
    public async set( id: string, name: string, value: any ): Promise<any> {
        DEBUG( `Set '%s' to '%s' for session '%s'`, name, value, id );

        const deferredRead = await this.read( id );

        if( !deferredRead ) {
            DEBUG( `Error setting '%s' to '%s', for session '%s': session not found`, name, value, id );

            return Promise.reject( new Error( `Error setting '${name}' to '${value}' for session '${id}': session not found. ` ) );
        }

        const session = _.set( deferredRead, name, value );

        const deferredWrite = await this.write( id, session );

        if( !deferredWrite ) {
            DEBUG( `Error setting '%s' to '%s' for session '%s': session could not be written to disk`, name, value, id );

            return Promise.reject( new Error( `Error setting '${name}' to '${value}' for session '${id}': session could not be written to disk. ` ) );
        }

        return Promise.resolve( session[name] );
    }
}