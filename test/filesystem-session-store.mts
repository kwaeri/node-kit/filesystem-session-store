/**
 * SPDX-PackageName: kwaeri/filesystem-session-store
 * SPDX-PackageVersion: 1.0.2
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


 'use strict'

 // INCLUDES
 import * as assert from 'assert';
 import { FilesystemSessionStore } from '../src/filesystem-session-store.mjs';


 // DEFINES
 let configuration = {
         version: "0.1.0",
         type: "filesystem",
         paths: {
            "cache": "data/cache"
         },
         store: {}
     },
     store = new FilesystemSessionStore( configuration ),
     sessionParams = {
         id: "0123456789",
         uk: "",
         host: "",
         domain: "",
         path: "",
         expires: 1585411327634,
         persistent: true,
         user: {
             username: "",
             email: "guest@example.com",
             name: {
                 first: "Test",
                 last: "User"
             }
         },
         authenticated: false
     },
     newSessionParams = {
         id: "1123456789",
         uk: "",
         host: "",
         domain: "",
         path: "",
         expires: new Date( new Date().getTime() + ( 30 * 60000 ) ).getTime(),
         persistent: true,
         user: {
             username: "",
             email: "guest@example.com",
             name: {
                 first: "Test",
                 last: "User"
             }
         },
         authenticated: false
     };


 // SANITY CHECK - Makes sure our tests are working proerly
 describe(
     'PREREQUISITE',
     () => {

         describe(
             'Sanity Test(s)',
             () => {

                 it(
                     'Should return true.',
                     async () => {
                         //const version = JSON.parse( ( await fs.readFile( path.join( './', 'package.json' ), { encoding: "utf8" } ) ) ).version;

                         //console.log( `VERSION: ${version}` );

                         return Promise.resolve(
                             assert.equal( [1,2,3,4].indexOf(4), 3 )
                         );
                     }
                 );

             }
         );

     }
 );


// Primary tests for the module
describe(
    'Session Store Functionality Test Suite',
    () => {


        describe(
            'Create Session Test',
            () => {

                it(
                    'Should return a newly stored session found by id.',
                    async () => {
                        const returned = await store.createSession( sessionParams.id, sessionParams );

                        return Promise.resolve(
                            assert.equal( JSON.stringify( sessionParams ), JSON.stringify( returned ) )
                        );
                    }
                );

            }
        );

        describe(
            'Get Session Test',
            () => {
                it(
                    'Should return a previously stored session found by id.',
                    async () => {
                        const returned = await store.getSession( sessionParams.id );

                        return Promise.resolve(
                            assert.equal( JSON.stringify( sessionParams ), JSON.stringify( returned ) )
                        );
                        }
                );
            }
        );

        describe(
            'Set Session Parameter Test',
            () => {
                it(
                    'Should return the value of a parameter set in a stored session found by id.',
                    async () => {
                        const returned = await store.set( sessionParams.id, 'authenticated', true );

                        return Promise.resolve(
                            assert.equal( true, returned )
                        );
                    }
                );
            }
        );

        describe(
            'Get Session Parameter Test',
            () => {
                it(
                    'Should return the value of a parameter previously set in a stored session found by id.',
                    async () => {
                        const returned = await store.get( sessionParams.id, 'authenticated', true );

                        return Promise.resolve(
                            assert.equal( true, returned )
                        );
                    }
                );
            }
        );

        describe(
            'Clean Sessions Test',
            () => {
                it(
                    'Should delete any session(s) which have an expires time stamp that is less than the current timestamp.',
                    async () => {
                        const ts                  = new Date(),
                                created             = await store.createSession( newSessionParams.id, newSessionParams ),
                                returned            = await store.cleanSessions();

                        return Promise.resolve(
                            assert.equal( await store.deleteSession( sessionParams.id ), false )
                        );
                    }
                );
            }
        );

        describe(
            'Delete Sessions Test',
            () => {
                it(
                    'Should delete the specified session from the session store.',
                    async () => {
                        return Promise.resolve(
                            assert.equal( await store.deleteSession( "1123456789" ), true )
                        );
                    }
                );
            }
        );


    }
);
